#! /user/bin/python3
# pip install itchat-uos  pip默认安装的那个版本已经不行了，安装git上master分支最新代码，目前可用。 pip install git+https://github.com/why2lyj/ItChat-UOS.git
# pip install xlsxwriter
# pip install Pillow
# pip install pandas  # 可以替换掉xlsxwriter
# pip install openpyxl #pandas 在处理xlsx 的时候依赖了openpyxl

"""
itchat-uos 测试微信登录
"""
import logging
from tkinter.font import names
import itchat
import xlsxwriter
import time
import json
from PIL import Image
import requests
from io import BytesIO
import os
import re
import pandas as pd

# 显示所有群组
def ListGroups():
    roomsList = itchat.get_chatrooms()
    for r in  roomsList:
        print(r['UserName'])
        print(r['NickName'])

def wechat_test():
    """
    # 测试微信登录
    """
    itchat.auto_login()
    itchat.send('Hello, filehelper', toUserName='filehelper')
    while True:
        try:
            key = input('选择功能: 1.测试文件组发消息 2.展示群  3.展示群成员  4.登出 5.展示群成员V1 6.测试头像采集\n')
            if key == '1':
                itchat.send('Hello, filehelper', toUserName='filehelper')
            elif key == '2':
                ListGroups()
            elif key == '3':                
                GetGroupMembers(0)
            elif key == '4':
                itchat.logout()
                break;
            elif key == '5':
                GetGroupMembers(1)
            elif key == '6':
                test_img()
        except Exception as e:
            logging.exception(e)
            print('出异常了')


def GetGroupMembers(savetype):
    """
    获取群成员

    savetype: 1.新版本的保存到execl；其他.旧保存方法
    """
    groupName = input('输入群名称\n')    
    rooms = itchat.search_chatrooms(name=groupName)
    if rooms is not None:
        room=rooms[0]
        gsq= itchat.update_chatroom(room['UserName'],detailedMember=True)
        if int(savetype) ==int(1):
            save_excel_v1(gsq,room);
        else:
            save_excel(gsq);
    else:
        print('未找到群')
    print('导出保存完成')   


def changeFileName(oldName):
    """
    正则表达式替换windows文件不能存储的特殊字符
    """
    pattern = re.compile(r'[\/\\\:\*\?\"\<\>\|]') # 要匹配的符号的正则表示形式
    new_name = re.sub(pattern, '_' , oldName) # 替换的模式，第二个参数是要替换成的目标字符，第三个参数是要替换的title
    new_name=new_name.replace('@','');
    return new_name

def save_excel(data):
    """
    群成员保存到excel
    """
    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook('demo.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.write('A1','UserName')
    worksheet.write('B1','NickName')
    worksheet.write('C1','HeadImgUrl')
    worksheet.write('D1','RemarkName')
    worksheet.write('E1','Sex')
    worksheet.write('F1','Signature')
    worksheet.write('G1','Province')
    worksheet.write('H1','City')
    worksheet.write('I1','EncryChatRoomId')
    worksheet.write('J1','EncryChatRoomId')
    num=int(2)
    for c in data['MemberList']:
        print(str(c));
        worksheet.write('A'+str(num),c['UserName'])
        worksheet.write('B'+str(num),c['NickName'])
        worksheet.write('C'+str(num),c['HeadImgUrl'])
        worksheet.write('D'+str(num),c['RemarkName'])
        worksheet.write('E'+str(num),c['Sex'])
        worksheet.write('F'+str(num),c['Signature'])
        worksheet.write('G'+str(num),c['Province'])
        worksheet.write('H'+str(num),c['City'])
        worksheet.write('I'+str(num),c['EncryChatRoomId'])
        worksheet.write('J'+str(num),time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) )
        num=num+1        
    workbook.close()


def save_excel_v1(data,room):
    """
    群成员保存到excel v1
    """
    timeStr=time.strftime("%Y%m%d%H%M%S", time.localtime())
    workbook = xlsxwriter.Workbook('members_'+timeStr+'.xlsx')
    worksheet = workbook.add_worksheet() 
    data=data['MemberList']
    num=int(0)    
    for c in data:
        c.pop('MemberList')
        if num==int(0):
            worksheet.write_row(0,0,c.keys());                                
        worksheet.write_row(num+1,0,c.values());
        img=itchat.get_head_img(userName=c["UserName"],chatroomUserName=room['UserName'])            
        dir=room['NickName'];
        if not os.path.exists(dir):
            os.makedirs(dir)

        picName=changeFileName(c['UserName'])+'.jpg'; 
        file_image=open(os.path.join(dir,picName),'wb')
        file_image.write(img)
        file_image.close()
        #worksheet.insert_image(num+1,0,)
        num=num+1
    workbook.close()
    print('完成了')


def getchatrooms():
    """
    获取所有群
    """
    roomList= itchat.get_chatrooms(update=True)[1:]
    return roomList;


def test_img():
    """
    测试获取群成员头像
    """
    roomsList=[]
    for i in getchatrooms():
        print(i)
        roomsList.append(i['UserName'])
        for n in roomsList:
            num=0
            if int(num)>10: #只测试10个头像
                return        
            chatRoom=itchat.update_chatroom(n,detailedMember=True)     
            for i in chatRoom['MemberList']:
                img=itchat.get_head_img(userName=i['UserName'],chatroomUserName=n)
                file_image=open(str(num)+'.jpg','wb')
                file_image.write(img)
                file_image.close()
                num+=1

def test_excel(data):
    """
    测试excel
    """
    workbook = xlsxwriter.Workbook('test_demo.xlsx')
    worksheet = workbook.add_worksheet()    
    num=int(0)    
    for c in data:
        if num==int(0):
            worksheet.write_row(0,0,c.keys());        
        worksheet.write_row(num+1,0,c.values());
        num=num+1
    workbook.close()
    print('完成了')


def json_test():
    """
    测试json
    """
    with open('testData.json',encoding='utf-8') as s:
        list= json.load(s);
        test_excel(list)
        # print(type(dd));
        # print(dd);

def changeMember(oldSheel,newSheel):
    """
    对比两个excel差异的用户
    """
    #读取excel数据
    dfa = pd.read_excel(oldSheel)
    dfb = pd.read_excel(newSheel)
    #获取所有用户名
    oldNames = dfa['UserName'].values
    print(len(oldNames))
    newName = dfa['UserName'].values
    print(len(newName))
    # 新进群的用户
    addName=list(set(newName).difference(set(oldNames)))  #newName中有，oldNames中没有
    print(addName)
    # 退群的用户
    quitName=list(set(oldNames).difference(set(newName)))  #a中有，b中没有   
    print(quitName)

# main函数
if __name__ == '__main__':
    wechat_test()
    #changeMember(r"D:\安美拉群统计\2022年8月13日\members_20220805160815.xlsx",r"D:\安美拉群统计\2022年8月5日\members_20220805160815.xlsx")    